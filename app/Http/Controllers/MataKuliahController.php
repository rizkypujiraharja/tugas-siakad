<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{MataKuliah,Dosen};

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliah = MataKuliah::with('dosen')->paginate();

        return view('matakuliah.index', compact('matakuliah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dosen = Dosen::get();
        return view('matakuliah.create', compact('dosen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|string|min:3|max:40',
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'sks' => 'required|min:1|max:8',
        ]);

        $matakuliah = new MataKuliah;
        $matakuliah->nama = $request->nama;
        $matakuliah->dosen_id = $request->dosen_id;
        $matakuliah->sks = $request->sks;
        $matakuliah->save();

        return redirect()->route('matakuliah.index')->with('alert-success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matakuliah = MataKuliah::findOrFail($id);
        $dosen = Dosen::get();
        return view('matakuliah.edit', compact('matakuliah', 'dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matakuliah = MataKuliah::findOrFail($id);

        $this->validate($request, [
            'nama' => 'required|string|min:3|max:40',
            'dosen_id' => 'required|numeric|exists:dosen,id',
            'sks' => 'required|min:1|max:8',
        ]);

        $matakuliah->nama = $request->nama;
        $matakuliah->dosen_id = $request->dosen_id;
        $matakuliah->sks = $request->sks;
        $matakuliah->save();

        return redirect()->route('matakuliah.index')->with('alert-success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matakuliah = MataKuliah::findOrFail($id);
        $matakuliah->delete();

        return redirect()->back()->with('alert-success', 'Data Berhasil Dihapus');
    }
}
