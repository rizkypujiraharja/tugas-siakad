<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AkademikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $akademik = User::where('level', 'akademik')->paginate();

        return view('akademik.index', compact('akademik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('akademik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|string|min:3|max:40',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $akademik = new User;
        $akademik->nama = $request->nama;
        $akademik->email = $request->email;
        $akademik->password = bcrypt($request->password);
        $akademik->level = 'akademik';
        $akademik->save();

        return redirect()->route('akademik.index')->with('alert-success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $akademik = User::findOrFail($id);
        return view('akademik.edit', compact('akademik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $akademik = User::findOrFail($id);
        $this->validate($request, [
            'nama' => 'required|string|min:3|max:40',
            'email' => 'required|email|unique:users,email,'.$akademik->id,
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        $akademik->nama = $request->nama;
        $akademik->email = $request->email;
        if(!is_null($request->password)){
            $akademik->password = bcrypt($request->password);
        }
        $akademik->save();

        return redirect()->route('akademik.index')->with('alert-success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $akademik = User::findOrFail($id);
        $akademik->delete();

        return redirect()->back()->with('alert-success', 'Data Berhasil Dihapus');
    }
}
