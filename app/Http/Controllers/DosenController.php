<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User,Dosen};

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::with('user')->paginate();

        return view('dosen.index', compact('dosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nidn' => 'required|numeric|min:6',
            'nama' => 'required|string|min:3|max:40',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = new User;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->level = 'dosen';
        $user->save();

        $dosen = new Dosen;
        $dosen->user_id = $user->id;
        $dosen->nidn = $request->nidn;
        $dosen->save();

        return redirect()->route('dosen.index')->with('alert-success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen = Dosen::with('user')->findOrFail($id);
        return view('dosen.edit', compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dosen = Dosen::findOrFail($id);
        $user = User::find($dosen->user_id);
        $this->validate($request, [
            'nama' => 'required|string|min:3|max:40',
            'email' => 'required|email|unique:users,email,'.$dosen->user_id,
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        $user->nama = $request->nama;
        $user->email = $request->email;
        if(!is_null($request->password)){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        $dosen->nidn = $request->nidn;
        $dosen->save();

        return redirect()->route('dosen.index')->with('alert-success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::findOrFail($id);
        $user = User::find($dosen->user_id);
        $dosen->delete();
        $user->delete();

        return redirect()->back()->with('alert-success', 'Data Berhasil Dihapus');
    }
}
