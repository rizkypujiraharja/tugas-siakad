<?php

namespace App\Http\Controllers;

use App\MateriKuliah;
use Illuminate\Http\Request;

class MateriKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MateriKuliah  $materiKuliah
     * @return \Illuminate\Http\Response
     */
    public function show(MateriKuliah $materiKuliah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MateriKuliah  $materiKuliah
     * @return \Illuminate\Http\Response
     */
    public function edit(MateriKuliah $materiKuliah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MateriKuliah  $materiKuliah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MateriKuliah $materiKuliah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MateriKuliah  $materiKuliah
     * @return \Illuminate\Http\Response
     */
    public function destroy(MateriKuliah $materiKuliah)
    {
        //
    }
}
