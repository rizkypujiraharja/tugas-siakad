@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('flash-message')
            <div class="card">
                <div class="card-header">Daftar Dosen</div>

                <div class="card-body">
                    <a href="{{ route('dosen.create') }}">
                        <button class="btn btn-primary">Tambah Dosen</button>
                    </a>
                    <br><br>
                    <table class="table">
                        <thead>
                          <tr>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                          <tr>
                        </thead>
                        <tbody>
                        @forelse ($dosen as $item)
                          <tr>
                              <td>{{ $item->nidn }}</td>
                              <td>{{ $item->user->nama }}</td>
                              <td>{{ $item->user->email }}</td>
                              <td>
                                  <form action="{{ route('dosen.destroy', $item) }}" id="{{$item->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{ route('dosen.edit', $item) }}" class="btn btn-sm btn-warning">
                                        Edit
                                    </a>
                                    <button class="btn btn-sm btn-danger">
                                        Delete
                                    </button>
                                  </form>
                              </td>
                          </tr>
                        @empty
                          <tr>
                              <td colspan="4" align="center">Belum ada data</td>
                          <tr>
                        @endforelse
                        </tbody>
                    </table>
                    <br><br>
                    {{ $dosen->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
