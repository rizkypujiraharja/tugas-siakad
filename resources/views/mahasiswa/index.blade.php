@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('flash-message')
            <div class="card">
                <div class="card-header">Daftar Mahasiswa</div>

                <div class="card-body">
                    <a href="{{ route('mahasiswa.create') }}">
                        <button class="btn btn-primary">Tambah Mahasiswa</button>
                    </a>
                    <br><br>
                    <table class="table">
                        <thead>
                          <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                          <tr>
                        </thead>
                        <tbody>
                        @forelse ($mahasiswa as $item)
                          <tr>
                              <td>{{ $item->nim }}</td>
                              <td>{{ $item->user->nama }}</td>
                              <td>{{ $item->user->email }}</td>
                              <td>
                                  <form action="{{ route('mahasiswa.destroy', $item) }}" id="{{$item->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{ route('mahasiswa.edit', $item) }}" class="btn btn-sm btn-warning">
                                        Edit
                                    </a>
                                    <button class="btn btn-sm btn-danger">
                                        Delete
                                    </button>
                                  </form>
                              </td>
                          </tr>
                        @empty
                          <tr>
                              <td colspan="4" align="center">Belum ada data</td>
                          <tr>
                        @endforelse
                        </tbody>
                    </table>
                    <br><br>
                    {{ $mahasiswa->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
