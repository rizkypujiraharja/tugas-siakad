@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Mata Kuliah</div>

                <div class="card-body">
                    <form action="{{ route('matakuliah.update', $matakuliah) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') ?? $matakuliah->nama }}" required autocomplete="nama" autofocus>

                                @if ($errors->has('nama'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="sks" class="col-md-4 col-form-label text-md-right">{{ __('SKS') }}</label>

                            <div class="col-md-6">
                                <input id="sks" type="text" class="form-control{{ $errors->has('sks') ? ' is-invalid' : '' }}" name="sks" value="{{ old('sks') ?? $matakuliah->sks }}" required autocomplete="sks">

                                @if ($errors->has('sks'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dosen_id" class="col-md-4 col-form-label text-md-right">{{ __('Dosen') }}</label>

                            <div class="col-md-6">
                                <select name="dosen_id" class="form-control{{ $errors->has('dosen_id') ? ' is-invalid' : '' }}">
                                    @forelse ($dosen as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $matakuliah->dosen_id ? 'selected' : '' }}>
                                        {{ $item->user->nama }}
                                    </option>
                                    @empty
                                        <option readonly>Tidak ada data</option>
                                    @endforelse
                                </select>
                                @if ($errors->has('dosen_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dosen_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Simpan') }}
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
