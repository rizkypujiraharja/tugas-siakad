@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('flash-message')
            <div class="card">
                <div class="card-header">Daftar Mata Kuliah</div>

                <div class="card-body">
                    <a href="{{ route('matakuliah.create') }}">
                        <button class="btn btn-primary">Tambah Mata Kuliah</button>
                    </a>
                    <br><br>
                    <table class="table">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nama</th>
                            <th>SKS</th>
                            <th>Dosen</th>
                            <th>Action</th>
                          <tr>
                        </thead>
                        <tbody>
                        @forelse ($matakuliah as $item)
                          <tr>
                              <td>{{ $item->id }}</td>
                              <td>{{ $item->nama }}</td>
                              <td>{{ $item->sks }}</td>
                              <td>{{ $item->dosen->user->nama }}</td>
                              <td>
                                  <form action="{{ route('matakuliah.destroy', $item) }}" id="{{$item->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{ route('matakuliah.edit', $item) }}" class="btn btn-sm btn-warning">
                                        Edit
                                    </a>
                                    <button class="btn btn-sm btn-danger">
                                        Delete
                                    </button>
                                  </form>
                              </td>
                          </tr>
                        @empty
                          <tr>
                              <td colspan="4" align="center">Belum ada data</td>
                          <tr>
                        @endforelse
                        </tbody>
                    </table>
                    <br><br>
                    {{ $matakuliah->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
