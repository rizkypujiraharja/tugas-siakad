<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriKuliahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materi_kuliah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('mata_kuliah_id')->unsigned();
            $table->string('nama');
            $table->string('file');
            $table->timestamps();

            $table->foreign('mata_kuliah_id')->references('id')->on('mata_kuliah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materi_kuliahs');
    }
}
