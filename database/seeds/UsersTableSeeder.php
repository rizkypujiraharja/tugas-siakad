<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->nama = 'Administrator';
        $user->email = 'admin@akademik.com';
        $user->password = bcrypt('secret123');
        $user->level = 'administrator';
        $user->save();
    }
}
